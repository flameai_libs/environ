import gc

from common.environ.settings_class import EnvironSettings


def print_environ() -> None:
    for var in gc.get_objects():
        if isinstance(var, EnvironSettings):
            for env_var_name in var.schema():
                print(f"Environ {env_var_name} setted as {var.schema()[env_var_name]}")
                # TODO: Make correct checking with monkeypatching
