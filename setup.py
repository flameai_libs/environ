from setuptools import setup


setup(
    name="common_environ",
    description="My pesonal code for using environment variables",
    version="v0.1.4",
    author="Alexander Andryukov",
    author_email="andryukov@gmail.com",
    install_requires=["pydantic-settings==2.2.1"],
    extras_require={
        "dev": ["bump-my-version==0.20.0", "ruff==0.3.5"],
        "tests": ["pytest==8.1.1"],
    },
)
