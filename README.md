# Библиотека для загрузки и использования переменных среды

## Пример использования:

```python
from common.environ.settings_class import EnvironSettings

class MySettings(EnvironSettings):
    MY_VAR: int = 1

```

при использовании в любом месте кода при старте приложения должна произойти валидация:
(TODO сделать валидацию)

requirements.txt

```text
--extra-index-url https://gitlab.com/api/v4/projects/flameai_libs%2Fpypi/packages/pypi/simple common-environ=={версия}
```

## Линтинг кода и код-стайл:

Установка для разработки

```sh
pip install -e ".[dev]"
```

Поднятие версии

```sh
bump-my-version bump {major/minor/patch} setup.py --tag --commit
```


Форматирование:

```sh
make format
```

Проверка:

```sh
make linting
```

