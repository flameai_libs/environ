import os
from unittest import mock
from typing import Generator

import pytest

from common.environ.settings_class import EnvironSettings

@pytest.fixture()
def env_string_value():
    return "some env string value"

@pytest.fixture()
def simple_env(env_string_value: str) -> Generator[str, None, None]:
    with mock.patch.dict(os.environ, {"env": env_string_value}):
        class MyEnvSettings(EnvironSettings):
            env: str

        yield MyEnvSettings().env

